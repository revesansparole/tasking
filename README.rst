========================
tasking
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/tasking/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/tasking/1.0.2/

.. image:: https://revesansparole.gitlab.io/tasking/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/tasking

.. image:: https://revesansparole.gitlab.io/tasking/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/tasking/

.. image:: https://badge.fury.io/py/tasking.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/tasking

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/tasking/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/tasking/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/tasking/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/tasking/commits/main
.. #}

Perform time simulation, using scheduled tasks to update current state

