:orphan:

.. Url of cited articles
    Use this file to create a reference to all the pdf fields stored in the directory
    so that you can quickly reference them in the docstrings of your function
    e.g.:
    - in this file: .. _`Allen et al. (1998)`: http://www.fao.org/3/X0490E/X0490E00.htm
    - in your module:
        """
        .. include:: ../user/biblio/biblio.rst
        """
        def myfunc():
        """does something
        Reference:
        - `Allen et al. (1998)`_
        """
