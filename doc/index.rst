.. {# pkglts, glabpkg_dev

Welcome to tasking's documentation!
====================================================

.. toctree::
    :caption: User's documentation
    :maxdepth: 2

    user/index
    _gallery/index

.. toctree::
    :caption: Contents
    :maxdepth: 2

    readme
    installation
    usage

.. toctree::
    :caption: Developer's documentation
    :maxdepth: 4

    Sources <_dvlpt/modules>

.. toctree::
    :caption: Annexe
    :maxdepth: 2

    gitlab home <https://gitlab.com/revesansparole/tasking>
    management

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. #}
